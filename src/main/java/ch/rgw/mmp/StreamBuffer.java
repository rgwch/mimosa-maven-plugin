package ch.rgw.mmp;

/**
 * Created by gerry on 03.07.15.
 */
import java.util.*;
import java.io.*;
class StreamBuffer extends Thread
{
    InputStream is;
    String type;

    StreamBuffer(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
    }

    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null)
                System.out.println(type + ">" + line);
            } catch (IOException ioe)
              {
                ioe.printStackTrace();
              }
    }
}