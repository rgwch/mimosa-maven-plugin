package ch.rgw.mmp;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Requirement;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by gerry on 02.07.15.
 */
@Mojo(name = "mimosa")
public class MimosaMavenPlugin extends AbstractMojo {

  @Parameter private File source;
  @Parameter private File intermediate;
  @Parameter private File dest;
  @Parameter private String mimosaOptions;


  public void execute() throws MojoExecutionException, MojoFailureException {
    if(dest==null || intermediate==null || source==null){
      throw new MojoExecutionException("parameter error");
    }

    if (!dest.exists() && !dest.mkdirs()) {
      throw new MojoFailureException(null, "could not create directory " + dest.getAbsolutePath(), "");
    }
    getLog().info("launching mimosa from " + source.getAbsolutePath());
    try {
      Process mimosa=Runtime.getRuntime().exec("mimosa build", null,source);
      if(mimosa==null){
        throw new MojoExecutionException("Error launching mimosa");
      }
      InputStream es=mimosa.getErrorStream();
      getLog().debug("es created "+es.toString());
      StreamBuffer errorStream=new StreamBuffer(es,"ERROR");
      InputStream is=mimosa.getInputStream();
      getLog().debug("is created "+is.toString());
      StreamBuffer outputStream=new StreamBuffer(is,"OUTPUT");
      errorStream.start();
      outputStream.start();
      getLog().info(Integer.toString(mimosa.waitFor()));
      FileUtils.copyDirectory(intermediate,dest);
    } catch (IOException e) {
      e.printStackTrace();
      throw new MojoFailureException(e.getMessage());
    } catch (InterruptedException e) {
      e.printStackTrace();
      throw new MojoFailureException(e.getMessage());
    }
  }
}
